#ifndef STACK_H
#define STACK_H

#include <stdlib.h>

typedef struct	s_stack
{
	struct s_stack	*next;
	struct s_stack	*prev;
	int				value;
}	t_stack;

/**		stack operations !
 *
 * 		- push:
 * 			- pa		: @brief		(push to stack <a>)
 * 								takes the top | last node from b and put it in the top of a
 * 			- pb		: @brief 	(push to stack <b>)
 * 								takes the top | last node from a and put it in the top of b
 *
 * 					  @note		do nothing if either <a> | <b> is empty / (NULL) !
 *
 * 		- swap:
 * 			- sa		: @brief 	(swap the stack <a>)
 * 								Swap the first 2 elements at the top of stack <a>.
 * 			- sb		: @brief 	(swap the stack <b>)
 * 								Swap the first 2 elements at the top of stack <b>.
 * 			- ss		: @brief 	(swap both the stack's <a> and <b>)
 * 								Swap the first 2 elements at the top
 * 								of both stack <a> and <b>.
 *
 * 					  @note		Do nothing if there is only one or no elements.
 *
 * 		- rotate
 * 			@brief	this is a visual example of
 * 					rotation of a stack named <S>
 *
 * 					 <S>					 <S>
 * 			first>	+---+				+---+
 * 					| 1 |				| 4 |
 * 					+---+				+---+
 * 					| 4 |	 rotate		| 2 |
 * 					+---+	-------->	+---+
 * 					| 2 |				| 6 |
 * 			last	>	+---+				+---+
 * 					| 6 |				| 1 |
 * 					+---+				+---+
 *
 * 			- ra		: @brief 	(rotate the stack <a>)
 * 								Shift up all elements of stack <a> by 1
 * 			- rb		: @brief 	(rotate the stack <b>)
 * 								Shift up all elements of stack <b> by 1
 * 			- rr		: @brief 	(rotate both the stack's <a> and <b>)
 * 								Shift up all elements of both stack <a> and <b> by 1
 *
 * 					  @note		the first element becomes the last one !
 *
 * 		- reverse rotate
 * 			@brief	this is a visual example of
 * 					reverse rotation of a stack named <S>
 *
 * 					 <S>					 <S>
 * 			first>	+---+				+---+
 * 					| 1 |				| 6 |
 * 					+---+				+---+
 * 					| 4 |	 rrotate		| 1 |
 * 					+---+	-------->	+---+
 * 					| 2 |				| 4 |
 * 			last	>	+---+				+---+
 * 					| 6 |				| 2 |
 * 					+---+				+---+
 *
 * 			- rra	: @brief		(reverse rotate the stack <a>)
 * 								Shift down all elements of stack <a> by 1
 * 			- rrb	: @brief		(reverse rotate the stack <b>)
 * 								Shift down all elements of stack <b> by 1
 * 			- rrr	: @brief		(reverse rotate both stack's <a> and <b>)
 * 								Shift down all elements of both stack <a> and <b> by 1
 *
 * 					  @note		the last element becomes the first !
*/

/// stack operations ! ///

/* push */
void		pa(t_stack *stack_a, t_stack *stack_b);
void		pb(t_stack *stack_a, t_stack *stack_b);

/* swap */
void		sa(t_stack *stack_a);
void		sb(t_stack *stack_b);
void		ss(t_stack *stack_a, t_stack *stack_b);

/* rotate */
void		ra(t_stack **stack);
void		rb(t_stack **stack);
void		rr(t_stack **stack_a, t_stack **stack_b);

/* reverse rotate */
void		rra(t_stack **stack);
void		rrb(t_stack **stack);
void		rrr(t_stack **stack_a, t_stack **stack_b);

#endif


























