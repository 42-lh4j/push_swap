#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include "stack.h"

/**
 *	@brief			this function add's a node <new_>
 * 					to the end of the stack
 * 					pointed by <head> .
 *	@param	head		pointer to the stack !
 * 	@param	new_		the new node ... !
 */
void		ps_list_add(t_stack *head, t_stack *new_);

/**
 *	@brief			this function return's a pointer
 * 					to the newly allocated node that
 *					hold's a value <content> ... !
 * 	@warning			heap allocated ... must be freed !
 * 	@param	content	a int which is required to be placed
 * 					on the node !
 * 	@return			`NULL` if the allocation failed !
 * 					otherwise it'll return a pointer
 * 					of the newly allocated node
 */
t_stack	*ps_list_new(int content);

/**
 *	@brief			this functions return's a pointer
 * 					of the last node in the stack
 * 					pointed to by <head> ... !
 * 	@param	head		pointer to the stack !
 * 	@return			`NULL` if NULL is passed as argument !
 * 					otherwise it'll return the last node !
 */
t_stack	*ps_get_last(t_stack *head);

/**
 *	@brief			this function calculates the length (size)
 * 					of the stack pointed to by <head> !
 * 	@param	head		pointer to the stack !
 * 	@return			a integer (size or length) of the stack !
 * 					0 if empty | in other word's `NULL` passed
 */
int		ps_get_stack_len(t_stack *head);

/// @brief			these function's are responsable for stack printing
///					simply the print_stack print's a single stack
///					ps (obviously no one's single here except U & I 😭💀)
///					the other one print's both stacks a & b ... !

void		put_stack(t_stack *head);
void		put_stacks(t_stack *a_head, t_stack *b_head);

#endif
