NAME	=	push_swap
CC	=	cc
CFLAGS	=	-Wall -Wextra -Werror
FILES	=	src/utils/functions/ps_add.c		\
		src/utils/functions/ps_last.c		\
		src/utils/functions/ps_new.c		\
		src/utils/functions/ps_print.c		\
		src/utils/functions/ps_slen.c		\
		src/utils/operations/push.c		\
		src/utils/operations/reverse_rotate.c	\
		src/utils/operations/rotate.c		\
		src/utils/operations/swap.c		\
		src/main.c
INCLUDE	=	include/
LIBDIR	=	lib/
LIBS	=	$(LIBDIR)/ft_printf/libftprintf.a	\
		$(LIBDIR)/libft/libft.a

all: mklibs $(NAME)

$(NAME): $(FILES)
	$(CC) $(CFLAGS) $^ $(LIBS) -I $(INCLUDE) -o $@

mklibs:
	@make -C $(LIBDIR)/ft_printf/
	@make -C $(LIBDIR)/libft/

clean:
	@make -C $(LIBDIR)/ft_printf/ clean
	@make -C $(LIBDIR)/libft/ clean

fclean: clean
	@rm -f $(NAME)
	@make -C $(LIBDIR)/ft_printf/ fclean
	@make -C $(LIBDIR)/libft/ fclean

re: fclean all
