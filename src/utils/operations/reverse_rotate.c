#include "../../../include/stack.h"
#include "../../../include/functions.h"

/**
 *	@brief			this function reverse rotates the stack <stack>
 * 					by shifting down all element's by 1
 * 	@note			see the illustration in `stack.h`
 * 					this is done by:
 * 						1.	setting the <new_head> to
 * 							the next of the current head
 * 							2.	setting the <new_head>->prev to NULL
 * 						(the head has no prev ... !)
 * 						3.	set the <last>->next to the current head
 * 	@note			yess it's frustrating to think and visualize
 * 					it that's why i said ... "see the
 * 					illustration in `stack.h`" !
 * 						4.	set the current head's next to NULL
 * 							(the last has no next ... !)
 * 						5.	set the current head's prev to the last'
 * 						6.	finally make the stack pointer point's
 * 							to the new head ... !
 *	@param	stack	a pointer to pointer that point's
 * 					to the head of the stack <stack> !!!
 */
static void		reverse_rotate(t_stack **stack)
{
	t_stack	*new_head;
	t_stack	*last;

	new_head = (*stack)->next;
	last = ps_get_last(*stack);
	new_head->prev = NULL;
	last->next = (*stack);
	(*stack)->next = NULL;
	(*stack)->prev = last;
	*stack = new_head;
}

/// @breif	see 'stack.h' for more info's
///	@note	these <rra> <rrb>, <rrr> functions
///			are all based on the <reverse_rotate> function above !
///	@note	if nothing passed the function(s) does nothing !

void		rra(t_stack **stack)
{
	if (!stack)
		return ;
	if (!*stack)
		return ;
	reverse_rotate(stack);
}

void		rrb(t_stack **stack)
{
	if (!stack)
		return ;
	if (!*stack)
		return ;
	reverse_rotate(stack);
}

void		rrr(t_stack **stack_a, t_stack **stack_b)
{
	rra(stack_a);
	rrb(stack_b);
}
