#include "../../../include/stack.h"
#include "../../../include/functions.h"

/**
 *	@brief			this function rotate's the stack <stack>
 * 					by shifting up all it's element's by 1
 * 	@note			see the illustration in `stack.h`
 * 					this is done by:
 * 						1.	makin the last->next point to the first
 * 						2.	and the last->prev->next point to NULL
 * 						3.	setting the passed pointer to pointer
 * 							to stack's head point's to the new head !
 *	@param	stack	a pointer to pointer that point's
 * 					to the head of the stack <stack> !!!
 */
static void	rotate(t_stack **stack)
{
	t_stack	*last;
	t_stack	*temp;

	last = ps_get_last(*stack);
	temp = last->prev;
	temp->next = NULL;
	last->next = *stack;
	last->prev = NULL;
	*stack = last;
}

/// @breif	see 'stack.h' for more info's
///	@note	these <ra> <rb>, <rr> functions
///			are all based on the <rotate> function above !
///	@note	if nothing passed the function(s) does nothing !

void		ra(t_stack **stack)
{
	if (!stack)
		return ;
	if (!*stack)
		return ;
	rotate(stack);
}

void		rb(t_stack **stack)
{
	if (!stack)
		return ;
	if (!*stack)
		return ;
	rotate(stack);
}

void		rr(t_stack **stack_a, t_stack **stack_b)
{
	ra(stack_a);
	rb(stack_b);
}
