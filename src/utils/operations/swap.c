#include "../../../include/stack.h"
#include "../../../include/functions.h"

/**
 *	@brief			this function swap's the first 2 nodes
 * 					at the _top_ of stack <stack> ... !
 * 					and this is done by:
 * 						1.	storing the top's first node's value in <temp>
 * 						2.	changing the top's first node's value
 * 							to the top's second node's value !
 * 						3.	then changing the top's second node's value
 * 							to the one stored in <temp> !
 * 	@param	stack	a pointer to the head of the stack !
 */
static void	swap(t_stack *stack)
{
	t_stack	*stack_last;
	int		temp;

	stack_last = ps_get_last(stack);
	temp = stack_last->value;
	stack_last->value = stack_last->prev->value;
	stack_last->prev->value = temp;
}

///	@brief	see 'stack.h' for more info's
/// @note	these <sa> <sb>, <ss> function's
///			are based on the <swap> function above !

void		sa(t_stack *stack_a)
{
	if (ps_get_stack_len(stack_a) <= 1)
		return ;
	swap(stack_a);
}

void		sb(t_stack *stack_b)
{
	if (ps_get_stack_len(stack_b) <= 1)
		return ;
	swap(stack_b);
}

void		ss(t_stack *stack_a, t_stack *stack_b)
{
	sa(stack_a);
	sb(stack_b);
}
