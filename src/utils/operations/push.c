#include "../../../include/stack.h"
#include "../../../include/functions.h"

/**
 *	@brief		this function pushes the last node from stack <src>
 * 				to the last node from the stack <dst> and this
 * 				is done by:
 * 					1.	making the last node from <dst>->next points
 * 						to the last node from <src> !
 * 					2.	making the last node from <src>->prev points
 * 						to the last node from <dst> !
 * 					3.	and finally making the previous node from stack
 * 						<src> point's to `NULL` !
 * 	@param	dst	the pointer to the stack which we
 * 				will push the node to it's _top_ !
 *	@param	src	the pointer to the stack which
 * 				it's last node will be pushed
 * 				to the _top_ of <dst> !
 */
static void	push(t_stack *dst, t_stack *src)
{
	t_stack	*last_dst;
	t_stack	*last_src;
	t_stack	*temp_;

	last_dst = ps_get_last(dst);
	last_src	 = ps_get_last(src);
	temp_ = last_src->prev;

	last_src->prev = last_dst;
	last_dst->next = last_src;
	temp_->next = NULL;
}

///	@brief	see 'stack.h' for more info's
/// @note	these <pa> <pb> function's
///			are based on the <push> function above !
///	@note	if nothing passed the function(s) does nothing !

void		pa(t_stack *stack_a, t_stack *stack_b)
{
	if (!stack_a || !stack_b)
		return ;
	push(stack_a, stack_b);
}

void		pb(t_stack *stack_a, t_stack *stack_b)
{
	if (!stack_a || !stack_b)
		return ;
	push(stack_b, stack_a);
}
