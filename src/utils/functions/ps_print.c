#include "../../../include/stack.h"
#include "../../../include/libftprintf.h"

void		put_stacks(t_stack *a_head, t_stack *b_head)
{
	ft_printf("\n  a  \t  b  \n");
	while (a_head || b_head)
	{
		ft_printf("+---+\t+---+\n| %i |\t| %i |\n",
				a_head ? a_head->value : 0,
				b_head ? b_head->value : 0
		);

		if (a_head)
			a_head = a_head->next;
		if (b_head)
			b_head = b_head->next;
	}
	ft_printf("+---+\t+---+");
}

void		put_stack(t_stack *head)
{
	while (head)
	{
		ft_printf("+---+\n| %i |\n", head->value);
		head = head->next;
	}
	ft_printf("+---+\n");
}
