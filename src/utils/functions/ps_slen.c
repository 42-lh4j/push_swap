#include "../../../include/stack.h"

int		ps_get_stack_len(t_stack *head)
{
	int	size;

	size = 0;
	if (!head)
		return (size);
	while (head)
	{
		head = head->next;
		size++;
	}
	return (size);
}
