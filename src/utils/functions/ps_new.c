#include "../../../include/functions.h"

t_stack	*ps_list_new(int content)
{
	t_stack	*new_;

	new_ = (t_stack *)malloc(sizeof(t_stack));
	if (!new_)
		return (NULL);
	new_->next = NULL;
	new_->prev = NULL;
	new_->value= content;
	return (new_);
}
