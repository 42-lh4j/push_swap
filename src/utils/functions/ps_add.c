#include "../../../include/functions.h"

void		ps_list_add(t_stack *head, t_stack *new_)
{
	t_stack	*last;

	last = ps_get_last(head);
	last->next = new_;
	new_->prev = last;
}
