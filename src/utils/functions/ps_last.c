#include "../../../include/functions.h"

t_stack	*ps_get_last(t_stack *head)
{
	if (!head)
		return (NULL);
	while (head->next)
		head = head->next;
	return (head);
}
